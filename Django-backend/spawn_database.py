import json
from requests import request


def create_skills(db_path: str, output_name: str = ""):
    with open(db_path, "r") as f:
        db = json.load(f)

    if type(db) != list:
        db = [db]

    prepared_json = list()
    tmp_dict = dict()
    for i in db:
        desc = i.get("desc", "N/A")
        desc = " ".join(desc) if desc != "N/A" else "N/A"
        tmp_dict = {
            "name": i.get("name", "N/A"),
            "description": desc,
            "stat": i.get("ability_score", {}).get("name", "N/A"),
        }
        prepared_json.append(tmp_dict)
    if output_name == "":
        return prepared_json
    with open(output_name + ".json", "w") as f:
        json.dump(prepared_json, f)


def create_stats(db_path: str, output_name: str = ""):
    with open(db_path, "r") as f:
        db = json.load(f)

    if type(db) != list:
        db = [db]

    prepared_json = list()
    tmp_dict = dict()
    for i in db:
        desc = i.get("desc", "N/A")
        desc = " ".join(desc) if desc != "N/A" else "N/A"
        tmp_dict = {
            "name": i.get("name", "N/A"),
            "full_name": i.get("full_name", "N/A"),
            "description": desc,
        }
        prepared_json.append(tmp_dict)
    if output_name == "":
        return prepared_json


def create_magic_school(db_path: str, output_name: str = ""):
    with open(db_path, "r") as f:
        db = json.load(f)

    if type(db) != list:
        db = [db]

    prepared_json = list()
    tmp_dict = dict()
    for i in db:
        tmp_dict = {
            "name": i.get("index", "N/A"),
            "description": i.get("desc", "N/A"),
        }
        prepared_json.append(tmp_dict)
    if output_name == "":
        return prepared_json
    with open(output_name + ".json", "w") as f:
        json.dump(prepared_json, f)


def create_classes(db_path: str, output_name: str = ""):
    with open(db_path, "r") as f:
        db = json.load(f)

    if type(db) != list:
        db = [db]

    prepared_json = list()
    tmp_dict = dict()
    for i in db:
        saving_throws = [j.get("name") for j in i.get("saving_throws")]
        tmp_dict = {
            "name": i.get("index", "N/A"),
            "hit_die": i.get("hit_die", "N/A"),
            "saving_throws": saving_throws,
        }
        prepared_json.append(tmp_dict)
    if output_name == "":
        return prepared_json
    with open(output_name + ".json", "w") as f:
        json.dump(prepared_json, f)


def create_spells(db_path: str, output_name: str = ""):
    with open(db_path, "r") as f:
        db = json.load(f)

    if type(db) != list:
        db = [db]

    prepared_json = list()
    tmp_dict = dict()
    for i in db:
        classes = [j.get("name") for j in i.get("classes")]
        desc = i.get("desc", "N/A")
        desc = " ".join(desc) if desc != "N/A" else "N/A"
        higher_level = i.get("higher_level", "N/A")
        higher_level = " ".join(higher_level) if higher_level != "N/A" else None
        components = i.get("components", "N/A")
        components = "".join(components) if components != "N/A" else "N/A"
        tmp_dict = {
            "name": i.get("index", "N/A"),
            "verbose_name": i.get("name", "N/A"),
            "description": desc,
            "higher_level": higher_level,
            "range": i.get("range", "N/A"),
            "components": components,
            "material": i.get("material", None),
            "ritual": i.get("ritual", "N/A"),
            "duration": i.get("duration", "N/A"),
            "concentration": i.get("concentration", "N/A"),
            "casting_time": i.get("casting_time", "N/A"),
            "level": i.get("level", "N/A"),
            "school_name": i.get("school", {}).get("index", "N/A"),
            "classes": classes,
        }
        prepared_json.append(tmp_dict)
    if output_name == "":
        return prepared_json

    with open(output_name + ".json", "w") as f:
        json.dump(prepared_json, f)


if __name__ == "__main__":
    server_link = "http://127.0.0.1:8000/apis/"
    list_creations = [
        create_stats,
        create_skills,
        create_magic_school,
        create_classes,
        create_spells,
    ]
    api_links = [
        "info/stats/new_stat/",
        "info/skills/new_skill/",
        "info/magic_schools/new_magic_school/",
        "info/classes/new_class/",
        "info/spells/new_spell/",
    ]
    db_pathes = [
        "Django-backend/5e-database/5e-SRD-Ability-Scores.json",
        "Django-backend/5e-database/5e-SRD-Skills.json",
        "Django-backend/5e-database/5e-SRD-Magic-Schools.json",
        "Django-backend/5e-database/5e-SRD-Classes.json",
        "Django-backend/5e-database/5e-SRD-Spells.json",
    ]
    bad_codes = list()
    for i, v in enumerate(list_creations):
        r = request("post", server_link + api_links[i], json=v(db_pathes[i]))
        if r.status_code != 200:
            bad_codes.append({v.__name__: r})

    if bad_codes:
        print("Errors in:", *bad_codes)
    else:
        print("Database created succesfully")
