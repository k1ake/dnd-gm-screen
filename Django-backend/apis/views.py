from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import (
    PlayerModel,
    SkillModel,
    StatModel,
    MagicSchoolModel,
    ClassModel,
    SpellModel,
)
import json


# =======================================
#               GET METHODS
# =======================================
@api_view(["GET"])
def getStatus(request):
    return Response("OK")


@api_view(["GET"])
def getApi(request):
    prefix_path = "apis/"
    response = {
        "available-apis": {
            "GET": [
                {prefix_path: "Check connection"},
                {prefix_path + "info/": "Retruns info about all APIs"},
                {prefix_path + "info/skills/": "Returns all skills in database"},
                {prefix_path + "info/stats/": "Returns all stats in database"},
                {
                    prefix_path
                    + "info/magic_schools/": "Returns all magic schools in database"
                },
                {prefix_path + "info/classes/": "Returns all classes in database"},
                {prefix_path + "info/spells/": "Returns all spells in database"},
                {prefix_path + "info/players/": "Returns all players in database"},
            ],
            "POST": [
                {
                    prefix_path
                    + "info/skills/new_skill": "Add new skill or list of skills to database"
                },
                {
                    prefix_path
                    + "info/stats/new_stat/": "Add new stat or list of stats to database"
                },
                {
                    prefix_path
                    + "info/magic_schools/new_magic_school/": "Add new magic school or list of magic schools to database"
                },
                {
                    prefix_path
                    + "info/classes/new_class/": "Add new class or list of classs to database"
                },
                {
                    prefix_path
                    + "info/spells/new_spell/": "Add new spell or list of spells to database"
                },
                {
                    prefix_path
                    + "info/players/new_player/": "Add new player or list of players to database"
                },
            ],
        }
    }
    return Response(response)


@api_view(["GET"])
def getAllSkills(request):
    skills = SkillModel.objects.all()
    return Response(skills.values())


@api_view(["GET"])
def getAllStats(request):
    stats = StatModel.objects.all()
    return Response(stats.values())


@api_view(["GET"])
def getAllMagicSchools(request):
    schools = MagicSchoolModel.objects.all()
    return Response(schools.values())


@api_view(["GET"])
def getAllClasses(request):
    classes = ClassModel.objects.all()

    response_list = list()
    for i in classes:
        tmp_dict = {
            "name": i.name,
            "hit_die": i.hit_die,
            "saving_throws": [j.name for j in i.saving_throws.all()],
        }
        response_list.append(tmp_dict)

    return Response(response_list)


@api_view(["GET"])
def getAllSpells(request):
    spells = SpellModel.objects.all()
    return Response(spells.values())


@api_view(["GET"])
def getAllPlayers(request):
    players = PlayerModel.objects.all()
    return Response(players.values())


# =======================================
#              POST METHODS
# =======================================
@api_view(["POST"])
def addNewStat(request):
    if type(request.data) != list:
        data = [request.data]
    else:
        data = request.data
    answers = list()
    for stat in data:
        answers.append(create_new_stat(stat))

    return Response(answers)


@api_view(["POST"])
def addNewSkill(request):
    if type(request.data) != list:
        data = [request.data]
    else:
        data = request.data

    answers = list()
    for skill in data:
        answers.append(create_new_skill(skill))

    return Response(answers)


@api_view(["POST"])
def addNewMagicSchool(request):
    if type(request.data) != list:
        data = [request.data]
    else:
        data = request.data

    answers = list()
    for school in data:
        answers.append(create_new_magic_school(school))
    return Response(answers)


@api_view(["POST"])
def addNewClass(request):
    if type(request.data) != list:
        data = [request.data]
    else:
        data = request.data

    answers = list()
    for class_ in data:
        answers.append(create_new_class(class_))
    return Response(answers)


@api_view(["POST"])
def addNewSpell(request):
    if type(request.data) != list:
        data = [request.data]
    else:
        data = request.data

    answers = list()
    for spell in data:
        answers.append(create_new_spell(spell))
    return Response(answers)


@api_view(["POST"])
def addNewPlayer(request):
    if type(request.data) != list:
        data = [request.data]
    else:
        data = request.data

    answers = list()
    for player in data:
        answers.append(create_new_player(player))
    return Response(answers)


# =========================================
#       Functions for retrieving info
# =========================================


def get_foreign_key_object(data: dict, ModelName: object, object_name: str) -> object:
    object_name = data.get(object_name, None)
    possible_object_names = [i["name"] for i in ModelName.objects.all().values()]
    print(object)

    if object_name in possible_object_names:
        class_model = ModelName.objects.get(name=object_name)
        return class_model
    else:
        return None


def get_manytomany_field_objects(
    data: dict, ModelName: object, object_name: str
) -> list:
    objects_from_data = data.get(object_name, None)
    if type(objects_from_data) != list:
        objects_from_data = [objects_from_data]

    possible_object_names = {
        i["name"].lower(): i["name"] for i in ModelName.objects.all().values()
    }
    objects_list = list()
    errors = dict()

    for obj in objects_from_data:
        if obj.lower() in possible_object_names.keys():
            objects_list.append(
                ModelName.objects.get(name=possible_object_names[obj.lower()])
            )
        else:
            errors[obj] = "Not found in database"
    return errors if errors else objects_list


def get_values_and_defaults(
    data: dict,
    needed_values: list,
    have_defaults: list,
    must_have_values: list,
    ModelName: object,
) -> dict:
    object_dict = {i: data.get(i, None) for i in needed_values}
    have_defaults = {
        i: ModelName._meta.get_field(i).get_default() for i in have_defaults
    }
    for k, v in have_defaults.items():
        if object_dict.get(k, None) is None:
            object_dict[k] = v

    empty_values = list()
    for i in must_have_values:
        if object_dict.get(i, None) is None:
            empty_values.append(i)
    if empty_values:
        return {
            "status": "rejected",
            "created_object": object_dict,
            "empty_values": empty_values,
        }
    return object_dict


# ========================================================
# Functions to handle creation of objects
# =======================================================


def create_new_stat(data: dict) -> None:
    needed_values = ["name", "description", "full_name"]
    have_defaults = []
    must_have_values = ["name", "description", "full_name"]

    new_one = get_values_and_defaults(
        data, needed_values, have_defaults, must_have_values, StatModel
    )
    if new_one.get("empty_values", None):
        return new_one
    new_stat = StatModel(**new_one)
    new_stat.save()
    return {new_stat.name: "Success"}


def create_new_skill(data: dict) -> None:
    needed_values = ["name", "description", "stat"]
    have_defaults = []
    must_have_values = ["name", "description", "stat"]
    skill_dict = get_values_and_defaults(
        data, needed_values, have_defaults, must_have_values, SkillModel
    )
    if skill_dict.get("empty_values", None):
        return skill_dict

    skill_dict["stat"] = skill_dict["stat"].upper()
    skill_dict["stat"] = get_foreign_key_object(skill_dict, StatModel, "stat")

    if skill_dict["stat"] is None:
        return {
            "error": f"stat:{data['stat']} is not in database, add stat before assigning skill to it"
        }
    new_skill = SkillModel(**skill_dict)
    new_skill.save()
    return "Success"


def create_new_magic_school(data: dict) -> None:
    needed_values = ["name", "description"]
    have_defaults = []
    must_have_values = ["name", "description"]
    school_dict = get_values_and_defaults(
        data, needed_values, have_defaults, must_have_values, SkillModel
    )
    if school_dict.get("empty_values", None):
        return school_dict
    new_magic_school = MagicSchoolModel(**school_dict)
    new_magic_school.save()
    return "Success"


def create_new_class(data: dict) -> None:
    needed_values = ["name", "hit_die", "saving_throws"]
    have_defaults = []
    must_have_values = ["name", "hit_die", "saving_throws"]
    class_dict = get_values_and_defaults(
        data, needed_values, have_defaults, must_have_values, SkillModel
    )
    if class_dict.get("empty_values", None):
        return class_dict
    saving_throws_list = get_manytomany_field_objects(data, StatModel, "saving_throws")
    if type(saving_throws_list) == dict:
        return f"Model was not created, because this saving_throws were not found: {saving_throws_list.values()}"
    class_dict.pop("saving_throws")
    new_class = ClassModel(**class_dict)
    new_class.save()
    new_class.saving_throws.add(*saving_throws_list)
    return "Success"


def create_new_spell(data: dict) -> None:
    needed_values = [
        "name",
        "description",
        "verbose_name",
        "higher_level",
        "range",
        "components",
        "material",
        "ritual",
        "duration",
        "concentration",
        "casting_time",
        "level",
    ]
    have_defaults = ["ritual", "concentration"]
    must_have_values = ["name"]
    spell_dict = get_values_and_defaults(
        data, needed_values, have_defaults, must_have_values, SpellModel
    )
    if spell_dict.get("empty_values", None):
        return spell_dict

    school_model = get_foreign_key_object(data, MagicSchoolModel, "school_name")
    spell_dict["school_name"] = school_model

    new_spell = SpellModel(**spell_dict)
    new_spell.save()

    classes_list = get_manytomany_field_objects(data, ClassModel, "classes")
    print(classes_list)
    new_spell.classes.add(*classes_list)
    return "Success"


def create_new_player(data: dict) -> None:
    class_model = get_foreign_key_object(data, ClassModel, "player_class")
    needed_values = [
        "name",
        "race",
        "level",
        "enable_xp_model",
        "xp",
        "armor",
        "health_max",
        "health_current",
        "health_temporary",
        "player_speed",
        "base_attack_damage",
        "inspiration",
        "perception",
        "alignment",
        "languages_and_proficiencies",
        "player_str",
        "player_dex",
        "player_con",
        "player_int",
        "player_wis",
        "player_cha",
    ]
    have_defaults = [
        "alignment",
        "health_max",
        "health_current",
        "health_temporary",
        "armor",
        "enable_xp_model",
        "player_speed",
        "inspiration",
        "perception",
        "player_str",
        "player_dex",
        "player_con",
        "player_int",
        "player_wis",
        "player_cha",
    ]
    must_have_values = [
        "name",
    ]
    player_dict = get_values_and_defaults(
        data, needed_values, have_defaults, must_have_values, PlayerModel
    )
    if player_dict.get("empty_values", None):
        return player_dict
    player_dict["player_class"] = class_model
    new_player = PlayerModel(**player_dict)
    new_player.update_proficiency()
    new_player.save()
    return "Success"
