from django.urls import path
from . import views


urlpatterns = [
    path("", views.getStatus),
    path("info/", views.getApi),
    path("info/skills/", views.getAllSkills),
    path("info/skills/new_skill/", views.addNewSkill),
    path("info/stats/", views.getAllStats),
    path("info/stats/new_stat/", views.addNewStat),
    path("info/magic_schools/", views.getAllMagicSchools),
    path("info/magic_schools/new_magic_school/", views.addNewMagicSchool),
    path("info/classes/", views.getAllClasses),
    path("info/classes/new_class/", views.addNewClass),
    path("info/spells/", views.getAllSpells),
    path("info/spells/new_spell/", views.addNewSpell),
    path("info/players/", views.getAllPlayers),
    path("info/players/new_player/", views.addNewPlayer),
]
