from django.db import models
from django.urls import reverse

# Create your models here.


class PlayerModel(models.Model):
    name = models.CharField(max_length=100, default="_")
    player_class = models.ForeignKey(
        "ClassModel", null=True, blank=False, on_delete=models.SET_NULL
    )
    race = models.CharField(max_length=50, default="Human")
    level = models.SmallIntegerField(
        default=1, null=False, choices=((i, i) for i in range(1, 21))
    )
    enable_xp_model = models.BooleanField(default=True)
    xp = models.PositiveSmallIntegerField(default=0, null=True)
    proficiency_bonus = models.PositiveSmallIntegerField(default=2, null=True)

    armor = models.PositiveSmallIntegerField(default=10)
    health_max = models.PositiveSmallIntegerField(default=0)
    health_current = models.PositiveSmallIntegerField(default=0)
    health_temporary = models.SmallIntegerField(default=0)

    player_speed = models.PositiveSmallIntegerField(default=30)
    base_attack_damage = models.CharField(
        max_length=10, blank=True, null=True, default="1d4"
    )
    inspiration = models.BooleanField(default=False)
    perception = models.PositiveSmallIntegerField(default=8)

    alignment_choices = (
        ("LG", "Lawfull Good"),
        ("NG", "Neutral Good"),
        ("CG", "Chaotic Good"),
        ("LN", "Lawfull Neutral"),
        ("TN", "True Neutral"),
        ("CN", "Chaotic Neutral"),
        ("LE", "Lawfull Evil"),
        ("NE", "Neutral Evil"),
        ("CE", "Chaotic Evil"),
        ("UA", "Unaligned"),
    )
    alignment = models.CharField(max_length=2, choices=alignment_choices, default="UA")

    languages_and_proficiencies = models.CharField(
        max_length=200, blank=True, null=True
    )

    player_str = models.PositiveSmallIntegerField(default=10)
    player_dex = models.PositiveSmallIntegerField(default=10)
    player_con = models.PositiveSmallIntegerField(default=10)
    player_int = models.PositiveSmallIntegerField(default=10)
    player_wis = models.PositiveSmallIntegerField(default=10)
    player_cha = models.PositiveSmallIntegerField(default=10)

    def calculate_level(self):
        xp_gates = [
            300,
            900,
            2_700,
            6_500,
            14_000,
            23_000,
            34_000,
            48_000,
            64_000,
            85_000,
            100_000,
            120_000,
            140_000,
            165_000,
            195_000,
            225_000,
            265_000,
            305_000,
            355_000,
        ]
        nearest = [i for i, v in enumerate(xp_gates) if self.xp - v >= 0]
        self.level = max(nearest) + 2 if nearest != [] else 1

    def update_proficiency(self):
        level_value = getattr(self, "level")
        self.proficiency_bonus = 2 + (level_value - 1) // 4

    def __str__(self):
        return self.name

    class Meta:
        db_table = ""
        managed = True
        verbose_name = "Модель игрока"
        verbose_name_plural = "Модели игроков"

    def get_absolute_url(self):
        """
        Returns the url to access a particular book instance.
        """
        return reverse("player-detail", args=[str(self.id)])


class StatModel(models.Model):
    name = models.CharField(max_length=3, blank=False, null=False, primary_key=True)
    full_name = models.CharField(max_length=10, blank=False, null=False)
    description = models.TextField(max_length=1000, blank=False, null=True)

    def __str__(self):
        return self.full_name

    class Meta:
        db_table = ""
        managed = True
        verbose_name = "StatModel"
        verbose_name_plural = "StatModels"


class SkillModel(models.Model):
    name = models.CharField(max_length=50, blank=False, null=False, primary_key=True)
    description = models.TextField(max_length=1000, blank=False, null=True)
    stat = models.ForeignKey(
        "StatModel", null=True, blank=False, on_delete=models.SET_NULL
    )

    class Meta:
        verbose_name = "skillmodel"
        verbose_name_plural = "skillmodels"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("skillmodel_detail", kwargs={"pk": self.pk})


class MagicSchoolModel(models.Model):
    name = models.CharField(max_length=50, blank=False, null=False, primary_key=True)
    description = models.TextField(max_length=1000, blank=False, null=False)

    class Meta:
        verbose_name = "MagicSchoolModel"
        verbose_name_plural = "MagicSchoolModels"

    def capitalized_name(self):
        return self.name.capitalize()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("magic_school_model_detail", kwargs={"pk": self.pk})


class SpellModel(models.Model):
    name = models.CharField(max_length=50, blank=False, null=False, primary_key=True)
    verbose_name = models.CharField(max_length=50, blank=False, null=True)
    description = models.TextField(max_length=1000, blank=False, null=True)
    higher_level = models.CharField(max_length=200, blank=True, null=True)
    range = models.CharField(max_length=50, blank=True, null=True)
    components = models.CharField(max_length=3, blank=True, null=True)
    material = models.CharField(max_length=100, blank=True, null=True)
    ritual = models.BooleanField(blank=True, null=True, default=False)
    duration = models.CharField(max_length=50, blank=True, null=True)
    concentration = models.BooleanField(blank=True, null=True, default=False)
    casting_time = models.CharField(max_length=50, blank=True, null=True)
    level = models.SmallIntegerField(blank=True, null=True)
    school_name = models.ForeignKey(
        "MagicSchoolModel", null=True, blank=True, on_delete=models.SET_NULL
    )
    classes = models.ManyToManyField("ClassModel")

    class Meta:
        verbose_name = "spellmodel"
        verbose_name_plural = "spellmodels"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("spellmodel_detail", kwargs={"pk": self.pk})


class ClassModel(models.Model):
    name = models.CharField(max_length=50, blank=False, null=False, primary_key=True)
    hit_die = models.SmallIntegerField()
    saving_throws = models.ManyToManyField("StatModel")

    class Meta:
        verbose_name = "classmodel"
        verbose_name_plural = "classmodels"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("classmodel_detail", kwargs={"pk": self.pk})
