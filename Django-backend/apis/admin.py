from django.contrib import admin

# Register your models here.
from .models import PlayerModel, SkillModel, StatModel, ClassModel, MagicSchoolModel, SpellModel
admin.site.register(PlayerModel)
admin.site.register(SkillModel)
admin.site.register(StatModel)
admin.site.register(ClassModel)
admin.site.register(MagicSchoolModel)
admin.site.register(SpellModel)