let i = 1;

function roll_dice(max) {
  let dice = document.getElementById("d" + max + "-img");
  let res = document.getElementById("d" + max + "-res");
  let rand_value = Math.random() * 1000;

  // Rotate objects
  let degree = 720 * i + Math.floor(rand_value / 10) - 50;

  dice.style.transition = 1000 + Math.floor(rand_value / 2) + "ms";
  res.style.transition = dice.style.transition;

  dice.style.rotate = degree + "deg";
  res.style.rotate = dice.style.rotate;

  // Change dice value
  res.textContent =
    Math.floor(Math.random() * max) +
    (Boolean([10, 100].includes(max)) ? 0 : 1);
  if (max == 100) {
    res.textContent = res.textContent.padStart(2, 0);
    console.log("max");
  }
  i++;
}
