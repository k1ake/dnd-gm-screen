// https://code-boxx.com/drag-drop-sortable-list-javascript/

function sorting_list(target) {
  target.classList.add("battle_sorting_list");
  let items = target.getElementsByClassName("battle-unit"),
    current = null;

  // (B) MAKE ITEMS DRAGGABLE + SORTABLE
  for (let i of items) {
    // (B1) ATTACH DRAGGABLE
    i.draggable = true;

    // (B2) DRAG START - YELLOW HIGHLIGHT DROPZONES
    i.ondragstart = (e) => {
      current = i;
      for (let it of items) {
        if (it != current) {
          it.classList.add("hint");
        }
      }
    };

    // (B3) DRAG ENTER - RED HIGHLIGHT DROPZONE
    i.ondragenter = (e) => {
      e.stopPropagation();
      if (i != current) {
        i.classList.add("active");
      }
    };

    // (B4) DRAG LEAVE - REMOVE RED HIGHLIGHT
    i.ondragleave = () => i.classList.remove("active");

    // (B5) DRAG END - REMOVE ALL HIGHLIGHTS
    i.ondragend = () => {
      for (let it of items) {
        it.classList.remove("hint");
        it.classList.remove("active");
      }
    };

    // (B6) DRAG OVER - PREVENT THE DEFAULT "DROP", SO WE CAN DO OUR OWN
    i.ondragover = (e) => e.preventDefault();

    // (B7) ON DROP - DO SOMETHING
    i.ondrop = (e) => {
      e.preventDefault();
      if (i != current) {
        let currentpos = 0,
          droppedpos = 0;
        for (let it = 0; it < items.length; it++) {
          if (current == items[it]) {
            currentpos = it;
          }
          if (i == items[it]) {
            droppedpos = it;
          }
        }
        if (currentpos < droppedpos) {
          i.parentNode.insertBefore(current, i.nextSibling);
        } else {
          i.parentNode.insertBefore(current, i);
        }
      }
    };
  }
}

function toggle_dead(e) {
  // Придумать как генерировать этот словарь
  let images = {
    "artificer.png": "src/classes/artificer.png",
    barbarian: "src/classes/barbarian.png",
    bard: "src/classes/bard.png",
    cleric: "src/classes/cleric.png",
    druid: "src/classes/druid.png",
    fighter: "src/classes/fighter.png",
    monk: "src/classes/monk.png",
    paladin: "src/classes/paladin.png",
    ranger: "src/classes/ranger.png",
    rogue: "src/classes/rogue.png",
    sorcerer: "src/classes/sorcerer.png",
    warlock: "src/classes/warlock.png",
    wizard: "src/classes/wizard.png",
    enemy1: "src/enemies/enemy1.png",
    enemy2: "src/enemies/enemy2.png",
    enemy3: "src/enemies/enemy3.png",
    enemy4: "src/enemies/enemy4.png",
    dead: "src/stats/skull.png",
  };
  let current_img = e.target.src.split("/").slice(-3).join("/");
  if (current_img !== images.dead) {
    e.target.src = images.dead;
    e.target.style.opacity = 1;
    e.target.dead = "dead";
  } else {
    e.target.src = images[e.target.id];
    e.target.dead = "not_dead";
  }
}

function toggle_status(e) {
  console.log(e.target.dead);
  if (e.which == 3 && e.target.dead != "dead") {
    e.target.style.opacity = e.target.style.opacity == 0.5 ? 1 : 0.5;
  }

  if (e.which == 2) {
    toggle_dead(e);
  }
}

function end_round() {
  let items = document.querySelectorAll(".battle-unit img");
  for (let i of items) {
    i.style.opacity = 1;
  }
}
