# DnD GM Screen

Django app written in python, that made for game-masters to simplificate their tracking of everything happen in game

## Usage

App contains simple pdf-viewer for your adventure's cheatsheet, dice roller, players stats with comments and enemies board which will have their hp, armor, speed etc..

## Preview

![preview](alpha-preview.png)


## TODO

- [x] Divide window for different activivties
- [x] Create dices and make them roll
- [x] Battle buffer
- [x] Players buffer
- [x] Pdf buffer
- [ ] Make pdf-preview (from test1 branch) work on chrome-based browsers
- [ ] Editing mode
- [ ] Game saving and loading
- [ ] Database page
- [ ] Help page
- [ ] About page
- [ ] Link django database to front-end

### Editing mode

Activated by pressing Edit button in right side of nav-bar. While active adds buttons to add/remove players/units in battle/dices or change loaded pdf. To disable press Edit button again

### Game saving and loading

Not figured it out yet. May be it will be done via creating django model for adventure were will be fields for each activity and current setup. Or some token/json file which will describe current state of the game

### Database page

Page with players, units, spells, classes, etc.. which are in database. Also should be used to add them so gm wouldn't spend time during adventure to create each thing when needed

## Contribution

Feel free to contribute in any way.

